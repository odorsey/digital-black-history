@extends('layouts.app')

@section('content')

<h1>Contact</h1>
<p>Want to learn more or say hi? Shoot me a message using the form below.</p>
<form action="{{ url('/contact') }}" method="post" class="contact">
  {{ csrf_field() }}
  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" name="name" class="form-control">
  </div>

  <div class="form-group">
    <label for="email">Email</label>
    <input type="email" name="email" class="form-control">
  </div>

  <div class="form-group">
    <label for="message">Message</label>
    <textarea name="content" class="form-control"></textarea>
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
