<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <!-- Google Analytics -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-139301273-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-139301273-2');
    </script> -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="A free, searchable online directory that contains digital Black History projects, focusing on Black History projects that may be of use to genealogists, historians, and family historians.">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/brands.css" integrity="sha384-i2PyM6FMpVnxjRPi0KW/xIS7hkeSznkllv+Hx/MtYDaHA5VcF0yL3KVlvzp8bWjQ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.13.2/themes/smoothness/jquery-ui.css">

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Including jQuery UI for autocomplete functionality -->
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.13.2/jquery-ui.min.js"></script>
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/custom.js')}}"></script>

    <title>{{config('app.name', 'Digital Black History')}}</title>

  </head>
  <body>
    @include('inc.navbar')
    <main role="main" class="container">
      @yield('content')
    </main>
    <footer class="footer">
      <div class="container">
        <p class="text-center">
          <i class="fas fa-copyright"></i> <?php echo date('Y'); ?> All Rights Reserved.
          Made with love and pride by <a href="https://oliviapeacock.com/">ODP</a>. <i class="fas fa-fist-raised"></i>
        </p>
        <p class="text-center">
          <a href="https://twitter.com/oliviacodes" target="_blank"><i class="fab fa-twitter fa-lg footer-link"></i></a>
          <a href="mailto:contact@digitalblackhistory.com"><i class="fas fa-envelope fa-lg footer-link"></i></a>
        </p>
      </div>
    </footer>
  </body>
</html>
