<h3><?php echo $projects->total(); ?> Search Result(s)</h3>
THIS IS ADVANCED VIEW
<table class="table table-hover" id="project-table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Project Name</th>
      <th scope="col">Description</th>
      <th scope="col">Creator(s)</i></th>
      <!-- <th scope="col">Project Status</th> -->
    </tr>
  </thead>
<tbody>
@if(count($projects) > 0)
  @foreach($projects as $project)
  <tr class="fromPagination_data">
    <td class="projectName">
      <a href="{{$project->link}}" target="_blank">{{$project->name}}
      </a>
    </td>
    <td class="desc">{{$project->description}}</td>
    <td class="creator">{{$project->creator}}</td>
  </tr>
  @endforeach
@elseif (count($projects < 1))
    <div class="alert alert-danger" role="alert">
      No projects found :(
    </div>
  @endif
</tbody>
</table>
<div class="loading"></div>
<div class="paging">
    {{ $projects->links() }}
</div>
