<nav class="navbar navbar-expand-md navbar-dark bg-primary">
  <a class="navbar-brand" href="/">{{config('app.name', 'Digital Black History')}}</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="/">Home<span class="sr-only">(current)</span></a>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" href="#">Help a Project</a>
      </li> -->
      <li class="nav-item">
        <a class="nav-link" href="https://forms.gle/1iy6isPRLPdq9SbG6" target="_blank">Submit a Project</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About</a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="/about">The Project</a>
          <a class="dropdown-item" href="/faqs">FAQs</a>
          <a class="dropdown-item" href="/support">Support</a>
        </div>
      </li>
    </ul>
      <ul class="nav navbar-nav navbar-right">
        <!-- Authentication Links -->
        @if (Auth::guest())
          <li><a class="nav-link" href="{{ route ('login') }}">Login</a></li>
          &nbsp;
        @else
          <li class="dropdown">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
              {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu dropdown-menu-right" role="menu">
              <li><a class="dropdown-item" href="{{ route ('dashboard') }}">Dashboard</a></li>
              <li>
                <a class="dropdown-item" href="{{ route ('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">Logout</a>

                <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
              </li>
        @endif
      </ul>
    </div>
  </div>
</nav>
