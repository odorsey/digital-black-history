@extends('layouts.app')


@section('content')
@if (Auth::guest())
  <h1>Sorry, no dice.</h1>
  Please <a href="{{ route ('login') }}">log in</a> to access this page.
@else
      <h1>Edit a Project</h1>
      <p>Choose a project from the dropdown below.</p>
        {!! Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'editProjectForm']) !!}

        <!-- ['action'=> 'ImportController@editProject',  -->
        <div class="edit-fields">
          <div class="form-group edit-field" id="country-0">
            <label>Project Title</label>
            <select id="inputProjectTitle" class="form-control col-md-6" name="inputProjectTitle[0]" aria-describedby="inputProjectTitle0Help">
                  <option selected="">1</option>
                  <option>2</option>
            </select>
            <small id="inputProjectTitle0Help" class="form-text text-muted">Choose a project from the dropdown.</small>
          </div>
        </div>
@endif
@endsection
