@extends('layouts.app')

@section('content')
  @if (Auth::guest())
    <h1>Sorry, no dice.</h1>
    Please <a href="{{ route ('login') }}">log in</a> to access this page.
  @else
      <h1>Add a Project</h1>
      <p>Use the form below to add a new project to the database.</p>
      {!! Form::open(['action'=> 'ImportController@uploadManualData', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group col-sm-12 col-md-8">
          <!-- Project Name -->
          <label for="inputProjectName">*Project Name</label>
          <input type="text" class="form-control" id="inputProjectName" name="inputProjectName" aria-describedby="inputProjectNameHelp" required>
          <small id="inputProjectNameHelp" class="form-text text-muted">Enter the name of the project.</small>

          <!-- Project Link -->
          <label for="inputProjectLink">*Project Link</label>
          <input type="url" class="form-control" id="inputProjectLink" name="inputProjectLink" aria-describedby="inputProjectLinkHelp" required>
          <small id="inputProjectLinkHelp" class="form-text text-muted">Enter the project link.</small>

          <!-- Project Description -->
          <label for="inputProjectDesc">*Project Description</label>
          <textarea class="form-control" id="inputProjectDesc" name="inputProjectDesc" aria-describedby="inputProjectDescHelp" rows="4" required></textarea>
          <small id="inputProjectDescHelp" class="form-text text-muted">Enter the project description.</small>

          <!-- Project Creator -->
          <label for="inputProjectCreator">*Project Creator</label>
          <input type="text" class="form-control" id="inputProjectCreator" name="inputProjectCreator" aria-describedby="inputProjectCreatorHelp" required>
          <small id="inputProjectCreatorHelp" class="form-text text-muted">Enter the project's creator (ex. an individual, group, or institution).</small>

          <!-- Status -->
          <label for="inputProjectStatus">Project Status</label>
          <select id="inputProjectStatus" class="form-control col-md-6" name="inputProjectStatus" aria-describedby="inputProjectStatusHelp">
                <option selected=""></option>
                <option>In-Progress</option>
                <option>Complete</option>
          </select>
          <small id="inputProjectStatusHelp" class="form-text text-muted">Enter the project's status.</small>

          <!-- Volunteers Needed? -->
          <label for="inputProjectVolunteers">Volunteers Needed?</label>
          <select id="inputProjectVolunteers" class="form-control col-md-6" name="inputProjectVolunteers" aria-describedby="inputProjectVolunteers">
                <option selected=""></option>
                <option>Yes</option>
                <option>No</option>
          </select>
          <small id="inputProjectVolunteersHelp" class="form-text text-muted">Does this project need volunteers?</small>

          <!-- How can someone help this project? -->
          <div class="form-group">
            <label for="inputProjectHelp">How to help?</label>
            <textarea class="form-control" id="inputProjectHelp" name="inputProjectHelp" aria-describedby="inputProjectHelpText" rows="4"></textarea>
            <small id="inputProjectHelpText" class="form-text text-muted">Enter information about how someone could help with this project. Do volunteers need to email someone?</small>
          </div>

          <!-- Project Needs -->
          <div class="form-group">
            <label for="inputProjectNeeds">Project Needs</label>
            <textarea class="form-control" id="inputProjectNeeds" name="inputProjectNeeds" aria-describedby="inputProjectNeedsHelp" rows="4"></textarea>
            <small id="inputProjectNeedsHelp" class="form-text text-muted">Enter any project needs here. What do they need help with?</small>
          </div>

          <!-- Project Creation Year -->
          <label for="inputProjectCreationYear">Project Creation Year</label>
          <input type="text" class="form-control w-25" id="inputProjectCreationYear" name="inputProjectCreationYear" aria-describedby="inputProjectCreationYearHelp">
          <small id="inputProjectCreationYearHelp" class="form-text text-muted">What year was the project created? (Example: 2018)</small>

          <!-- Project Time Period (Start Year) -->
          <label for="inputProjectStartYear">Project Time Period (Start Year)</label>
          <input type="text" class="form-control w-25" id="inputProjectStartYear" name="inputProjectStartYear" aria-describedby="inputProjectStartYearHelp">
          <small id="inputProjectStartYearHelp" class="form-text text-muted">What is the START YEAR for the time period that the project covers? (Example: 1850)</small>

          <!-- Project Time Period (End Year) -->
          <label for="inputProjectEndYear">Project Time Period (End Year)</label>
          <input type="text" class="form-control w-25" id="inputProjectEndYear" name="inputProjectEndYear" aria-describedby="inputProjectEndYearHelp">
          <small id="inputProjectEndYearHelp" class="form-text text-muted">What is the END YEAR for the time period that the project covers? (Example: 1872)</small>
        </div>

        <div class="add-project-btns">
          <a href="{{ route ('dashboard') }}" class="btn btn-secondary float-left" role="button">Cancel</a>
          <button type="submit" class="btn btn-primary float-right">Next</button>
        </div>
      </form>
    @endif

      <!-- <p>Upload a CSV file to add projects.</p> -->

      <!-- Create form to upload file -->
      <!-- {!! Form::open(['action'=> 'ImportController@uploadData', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="input-group" id="bulk-upload">
          {{ Form::file('file-import') }}
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form> -->

      <!-- Receive the file -->

      <!-- Display log of actions -->

      <!-- Parse the file, security checks, convert to MySQL -->

      <!-- Sent to database -->

      <!-- Return success message -->
      <!-- Or return error message -->
      <!-- @if(Session::has('message')) -->
        <p>{{ Session::get('message') }}</p>
      <!-- @endif -->

@endsection
