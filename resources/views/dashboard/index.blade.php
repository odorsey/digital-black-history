@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Dashboard</div>


            <div class="card-body">
              @if (\Session::has('success'))
                <div class="alert alert-success">
                  {!! \Session::get('success') !!}
                </div>
            @endif

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <p>Welcome, <span class="username">{{ Auth::user()->name }}</span>.</p>
                <p>This is the admin dashboard. Here, you can upload new projects to the database.</p>
                <p><a href="/dashboard/add-project">Add a new project manually</a></p>
                <!-- <p><a href="/dashboard/edit-project">Edit an existing project</a></p> -->
            </div>
        </div>
    </div>
</div>

@endsection
