@extends('layouts.app')

@section('content')
  @if (Auth::guest())
    <h1>Sorry, no dice.</h1>
    Please <a href="{{ route ('login') }}">log in</a> to access this page.
  @else
      <h1>Add a Project</h1>
      {!! Form::open(['action'=> 'ImportController@uploadManualData2', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'locationsform']) !!}

        <!-- Project Name -->
        @foreach($newProject as $project)
          <h2 id="{{$project->project_id}}" name="{{$project->project_id}}">Project Name: {{$project->name}}<!-- echo the name of the Project that was added--></h2>
        @endforeach

        <!-- Country 1 -->
        <div class="country-fields col-sm-12 col-md-8">
          <div class="form-group country-field" id="country-0">
            <label>Project Country</label>
            <select id="inputProjectCountry0" class="form-control col-md-6" name="inputProjectCountry[0]" aria-describedby="inputProjectCountry0Help">
                  <option selected=""></option>
                  @foreach($countries as $country)
                    <option>{{$country->area_name}}</option>
                  @endforeach
            </select>
            <small id="inputProjectCountry0Help" class="form-text text-muted">Enter the name of the country the project focuses on.</small>
          </div>
          <!-- Add a Country: on click, show another Country field -->
          <div class="row add-a-country">
            <button type="submit" class="btn btn-secondary float-left" id="add-country"><i class="fa fa-plus" aria-hidden="true"></i> Add a Country</button>
          </div>
        </div>



          <!-- State -->
          <div class="state-fields col-sm-12 col-md-8">
            <div class="form-group state-field" id="state-0">
              <label>Project State</label>
              <select id="inputProjectState0" class="form-control col-md-6" name="inputProjectState[0]" aria-describedby="inputProjectState0Help">
                    <option selected=""></option>
                    @foreach($states as $state)
                      <option>{{$state->area_name}}</option>
                    @endforeach
              </select>
              <small class="form-text text-muted">Enter the name of the state the project focuses on.</small>
            </div>

            <!-- Add a State: on click, show another State field -->
            <div class="row add-a-state">
              <button type="submit" class="btn btn-secondary float-left" id="add-state"><i class="fa fa-plus" aria-hidden="true"></i> Add a State</button>
            </div>
          </div>

          <!-- Tags -->
          <div class="tag-fields col-sm-12 col-md-8">
            <div class="form-group tag-field" id="tag-0">
              <label>Project Tags</label>
              <textarea class="form-control" id="inputProjectDesc" name="inputTags" aria-describedby="inputProjectTagsHelp" rows="4"></textarea>
              <small id="inputProjectTagsHelp" class="form-text text-muted">Enter keywords that describe the project. Separate each word by a comma.</small>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <button type="submit" class="btn btn-secondary float-left">Cancel</button>
            </div>

            <!-- On click, perform an insert statement on areas_projects database and tags_projects database -->
            <div class="col">
              <button type="submit" class="btn btn-primary float-right">Add Project</button>
            </div>
          </div>
        </div>
      </form>

<script type="text/javascript">

$(function(){
var countryID = 1;
var stateID = 1;

// functionality for additional country fields
$('#add-country').on('click', function(e) {
  e.preventDefault();
  // clone the first, original field and add it to the end of the parent div, .country-fields
  var countries = $('#country-0').clone().appendTo('.country-fields');

  // increment the ID each time a new field is created
  var newCountryID = countryID++;

  // find the id of the field and set it to 'country-' + the new incremented ID
  countries.attr('id', 'country-' + newCountryID);

  // find the child select tag field and set its name to 'inputProjectCountry[-' + the new incremented ID + ']'
  countries.children('select').attr('name', 'inputProjectCountry[' + (newCountryID) + ']');

    // find the child select tag field and set its id to 'inputProjectCountry' + the new incremented ID
  countries.children('select').attr('id', 'inputProjectCountry' + newCountryID);
});

// functionality for additional state fields
  $('#add-state').on('click', function(e) {
    e.preventDefault();
    var states = $('#state-0').clone().appendTo('.state-fields');
    var newStateID = stateID++;
    states.attr('id', 'state-' + newStateID);
    states.children('select').attr('name', 'inputProjectState[' + (newStateID) + ']');
    states.children('select').attr('id', 'inputProjectState' + newStateID);
  });
});

$(document).ready(function() {
    $("#inputTags").autocomplete({
        source: function(request, response) {
            $.ajax({
            url: siteUrl + '/' +"add-project-2",
            data: {
                    term : request.term
             },
            dataType: "json",
            success: function(data){
               var resp = $.map(data,function(obj){
                    return obj.name;
               });

               response(resp);
            }
        });
    },
    minLength: 2
 });
});
</script>

@endif
@endsection
