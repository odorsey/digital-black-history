@extends('layouts.app')

@section('content')
  <div>
    <div class="search-group">
      <h1>Search for a Black History Project</h1>
      <p class="intro">This website is a free, searchable directory for online history projects that can help further Black History research. This ongoing project was created to collect information about these digital Black History projects in order to benefit historians, genealogists, and family historians who are researching the lives of Black individuals and families.</p>
    </div>
  <div class="row">
    <div class="col-md-3">
      <div class="advanced-search">
        <form id="advanced">
          {{ csrf_field() }}
            <div class="form-group">
              <label>City</label>
              <input type="text" class="form-control" name="city" placeholder="" id="city">
            </div>

            <div class="form-group">
              <label>Country</label>
              <select id="country" class="form-control" name="country">
                <option selected></option>
                @foreach($countries as $country)
                  <option>{{$country->area_name}}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label>State</label>
              <select id="state" class="form-control" name="state">
                <option selected></option>
                @foreach($states as $state)
                  <option>{{$state->area_name}}</option>
                @endforeach
              </select>
            </div>

            <label>Time Period (Range)</label>
            <div class="row">
              <div class="col">
                <input type="text" class="form-control" name="timerange1" placeholder="From" id="timerange1">
              </div>
              <div class="col">
                <input type="text" class="form-control" name="timerange2" placeholder="To" id="timerange2">
              </div>
            </div>
            <div class="form-group advanced-submit">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-secondary advanced-submit-btn" id= "advanced-submit">
                  Search <i class="fas fa-search"></i>
                </button>
              </span>
            </div>
        </form>
      </div>
    </div>

    <div class="col-md">
      <form id="keyword-search">
      <div class="row">
        <div class="col keyword">
          <input type="text" class="form-control" name="keyword" placeholder="" id="keyword">
        </div>
        <span class="input-group-btn">
          <button type="submit" class="btn btn-secondary advanced-submit-btn" id = "keyword-advanced-submit">
            <i class="fas fa-search"></i>
          </button>
        </span>
      </div>
    </form>
      <div class="resultContent table-responsive-sm">
    <!-- Load table here -->
      @if(count($projects) > 0)
        @include('data')
      </div>
      @else
        <div class="alert alert-danger" role="alert">
          No projects found :(
        </div>
      @endif
    </div>
  </div>
  <div class="row top">
    <a href="#" id="scroll"><i class="fas fa-chevron-up fa-2x arrow"></i></a>
  </div>
</div>

<script type="text/javascript">
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function getAdvancedResults($state, $page) {
  // if page is set to 0 or undefined, then set to 1
  $page = $page || 1;
  $('.loading').show();

  // setting search variables
  $keyword = $("#keyword").val();
  $city = $("#city").val();
  $country = $("#country").val();
  $state = $("#state").val();
  $timerange1 = Number($("#timerange1").val());
  $timerange2 = Number($("#timerange2").val());

  $.ajax({
    type: 'GET',
    url: '{{URL::to('search')}}',
    data: {
          'keyword': $keyword,
          'city': $city,
          'country': $country,
          'state': $state,
          'timerange1': $timerange1,
          'timerange2': $timerange2,
          'page': $page
    },

    success:function(data){
      $('.loading').show();
      $('.resultContent').empty();
      $('.resultContent').html(data);
      $('.paging a').each(function (i, a) {
        a = $(a);
        console.log(a.attr("href"));
        a.attr("href", a.attr("href")+"&keyword="+$keyword+"&state="+$state) // need to fix for pagination links
      })
    },
  error: function(err) {
    console.log(err);
    // $('.loading').hide();
    $('.resultContent').empty();
    $('.resultContent').html('<h3>0 Search Results</h3><p>Sorry, no results here! Please try a different search.</p>');
  }
  });
}

// function for advanced search
// on click of "Search," load the results within page
$(function() {
  $('body').on('click', '.paging a', function(e) {
          e.preventDefault();

          $state = $("#state").val();

          page = getParameterByName("page", this.href)

          getAdvancedResults($state, page);
        });
});

$(function(page) {
  $('.advanced-submit-btn').on('click', function(e){
    console.log("advanced search clicked");

    // This resets the URL when advanced search performed
    history.replaceState('', 'Index', '/');

    // don't load to advanced page
    e.preventDefault();

    // get the parameters for advanced search
    $state = $("#state").val();

    // find the page
    page = page || 1;

    // display loading
    $('.loading').show();

    // get the results
    getAdvancedResults($state, page);
  });
});

// scroll to top
jQuery(document).ready(function () {
      jQuery(window).scroll(function(){
          if (jQuery(this).scrollTop() > 100) {
              jQuery('#scroll').fadeIn();
          } else {
              jQuery('#scroll').fadeOut();
          }
      });
      jQuery('#scroll').click(function(){
          jQuery("html, body").animate({ scrollTop: 0 }, 600);
          return false;
      });
});

</script>
@endsection
