<h3><?php echo $projects->total(); ?> Search Result(s)</h3>
SEARCH VIEW
<table class="table table-hover" id="project-table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Project Name</th>
      <th scope="col">Project Link</th>
      <th scope="col">Description</th>
      <th scope="col">Creator(s)</i></th>
      <!-- <th scope="col">Project Status</th> -->
    </tr>
  </thead>
<tbody>
  @foreach($projects as $project)
  <tr class="fromPagination_data">
    <td class="projectName">{{$project->name}}</td>
    <td class="projectLink">
      <a href="{{$project->link}}" target="_blank">
        {{$project->link}}
      </a>
    </td>
    <td class="desc">{{$project->description}}</td>
    <!-- <td class="creator">{{$project->creator}}</td> -->
  </tr>

  @endforeach
</tbody>
</table>
<div class="paging">
    {{ $projects->links() }}
</div>
