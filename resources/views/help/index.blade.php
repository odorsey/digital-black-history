@extends('layouts.app')

@section('content')
  <h1>Help a Project</h1>
  <p>Many of the projects in this directory need support in order to continue their work! Those seeking volunteers listed below.</p>
  <p>If you have the time or the means, consider checking out the Digital Black History projects below and reaching out to volunteer.</p>
  
    <table class="table table-hover" id="project-table">
      <thead class="thead-light">
        <tr>
          <th scope="col">Project Name</th>
          <th scope="col">Description</th>
          <th scope="col">Creator(s)</th>
        </tr>
      </thead>
      <tbody>
        @if(count($projects) > 0)
          @foreach($projects as $project)
            <tr>
              <th scope="row">
                <a href="{{$project->link}}" target="_blank">
                  {{$project->name}}
                </a>
              </th>
              <td>{{$project->description}}</td>
              <td>{{$project->creator}}</td>
            </tr>
          @endforeach
        @else
          <p>No projects found :(</p>
        @endif
    </tbody>
   </table>

   <div class="paging">
       {{ $projects->links() }}
   </div>
@endsection
