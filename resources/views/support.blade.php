@extends('layouts.app')

@section('content')
      <h1>{{$title}}</h1>
      <p>Thank you for your interest in helping out! There are a few ways you can help with <a href="http://digitalblackhistory.com">Digital Black History</a>.</p>
      <ol>
        <li>
          <span class="miniheader">Contribute to this directory.</span>
          <p>You can do this by accessing the <a href="https://forms.gle/1iy6isPRLPdq9SbG6" target="_blank">Google Form</a>. This is how volunteers enter data for upload to the site.</p>
        </li>
        <li>
          <span class="miniheader">Tell your friends.</span>
          <p>If this resource has been useful to you, share the link and tell others about it!</p>
        </li>
        <li>
          <span class="miniheader">Volunteer on a project in the directory.</span>
          <p>If you have the time, find a digital history project that needs volunteers and reach out to extend your services!</p>
        </li>
      </ol>
      <h2>Donate</h2>
      <p>If you've found the site useful for your own research, consider contributing to <a href="https://paypal.me/OliviaD" target="_blank">my PayPal</a> to help support <a href="http://digitalblackhistory.com">Digital Black History</a> and any future projects I create!</p>

@endsection
