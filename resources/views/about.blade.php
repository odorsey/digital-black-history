@extends('layouts.app')

@section('content')
      <h1>{{$title}}</h1>
      <p>Digital Black History was created out of a need for a centralized directory of all digital history projects relating to Black History. These projects have the potential to serve as valuable resources for family historians, genealogists, and historians alike. By putting them in one place, it will help make the research process more efficient for those doing historical research on Black people. All Black History projects within this directory are free to access.</p>

      <p>The directory is also an attempt to bring attention to these digital history projects, which may need volunteers or funding to sustain their efforts.</a>

      <p>This work builds upon the thoughtfully curated <a href="https://docs.google.com/document/d/1rZwucjyAAR7QiEZl238_hhRPXo5-UKXt2_KCrwPZkiQ/edit#">"Black Digital Humanities Projects & Resources"</a> document, started by the <a href="https://twitter.com/CCP_org"> Colored Conventions Project</a>.</p>

      <h2>Creator</h2>
      <p>This project was created by <a href="https://oliviapeacock.com/">Olivia Dorsey Peacock</a>.</p>

      <h2>Disclaimer</h2>
      <p>All Black History projects listed in the directory are property of their original owners.</p>

      <h2>Contact</h2>
      <p>Want to learn more or just say hi? Feel free to send me a message at <span class="bold"><a href="mailto:contact@digitalblackhistory.com">contact [at] digitalblackhistory.com</a></span>!</p>
@endsection
