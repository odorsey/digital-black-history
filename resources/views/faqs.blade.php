@extends('layouts.app')

@section('content')
      <h1>{{$title}}</h1>
      <ul class="about-list">
        <li>
          <h5 class="miniheader">This is nice and all, but "such and such" project isn't included in this directory...</h5>
          <p>No problem. I'd love to include it! Just fill out the <a href="https://forms.gle/1iy6isPRLPdq9SbG6" target="_blank">Google Form</a> with basic information about the digital Black History project and it will be added to the directory the next time the data is uploaded to the site.</p>
        </li>
        <li>
          <h5 class="miniheader">You've listed my project in this directory. Can you please take it off your site?</h5>
          <p>Sure thing! Send an email <a href="mailto:contact@digitalblackhistory.com">here</a> and I'll remove it as soon as possible.</p>
        </li>
        <li>
          <h5 class="miniheader">Is this site really completely free to use?</h5>
          <p>Yes, it is and always will be. I believe that resources used to research Black History should be free for everyone. However, if you've gotten some use out of this site, please consider donating to my <a href="https://paypal.me/OliviaD" target="_blank">PayPal</a>. The funds will help support the upkeep of the site and keep it free for everyone!</p>
        </li>
      </ul>

@endsection
