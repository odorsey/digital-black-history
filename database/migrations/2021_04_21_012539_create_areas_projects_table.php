<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas_projects', function (Blueprint $table) {
            $table->bigInteger('area_id')->unsigned();
            $table->foreign('area_id')->references('area_id')->on('areas');
            $table->bigInteger('project_id')->unsigned();
            $table->foreign('project_id')->references('project_id')->on('projects');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas_projects');
    }
}
