<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tags_projects', function (Blueprint $table) {
          $table->bigInteger('tag_id')->unsigned();
          $table->foreign('tag_id')->references('id')->on('tags');
          $table->bigInteger('project_id')->unsigned();
          $table->foreign('project_id')->references('project_id')->on('projects');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags_projects');
    }
}
