## About Digital Black History

Digital Black History is a searchable directory for online projects that can help further Black History research. This ongoing project was created to collect information about these digital projects in order to benefit genealogists and historians who are researching the lives of Black individuals and families.

http://digitalblackhistory.com/

## How to Run
`php artisan serve`
