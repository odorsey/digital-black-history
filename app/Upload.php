<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    public static function insertData($data) {
      // check to see if project is already in DB
      $value = DB::table('projects')->where('name', $data['name'])->get();

      if($value->count() == 0) {
        // if it's not already in the DB, then insert it
        DB::table('projects')->insert($data);
      }
    }

    public static function insertDataManual($data) {
      // check to see if project is already in DB
      $value = DB::table('projects')->where('name', $data['name'])->get();

      if($value->count() == 0) {
        // if it's not already in the DB, then insert it
        DB::table('projects')->insert($data);
      }
    }

    // Insert the location and tags data
    public static function insertProjectLocation($data){
      DB::table('areas_projects')->insert($data);
    }

    // Insert new tag ID data
    public static function insertTagIDs($data){
      DB::table('tags')->insert($data);
    }

    // Insert tags data
    public static function insertProjectTags($data){
      DB::table('tags_projects')->insert($data);
    }
}
