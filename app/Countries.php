<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{

    // Table Name
    protected $table = 'countries';

    // Primary key
    public $primarykey = 'country_id';

    // Timestamps
    public $timestamps = true;
}
