<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    // Table Name
    protected $table = 'projects';

    // Primary key
    public $primarykey = 'project_id';

    // Timestamps
    public $timestamps = true;
}
