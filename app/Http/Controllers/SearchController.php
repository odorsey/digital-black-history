<?php

namespace App\Http\Controllers;
use App\Project;
use Illuminate\Http\Request;

use DB;

class SearchController extends Controller
{

// Show the full dataset, paginated on first load.
public function index(Request $request)
{
  $projects = Project::orderBy('name', 'asc')->paginate(8);

  if ($request->ajax()){
    return view('data', ['projects' => $projects])->render();
  }

  // Show all countries for advanced search
  $countries = DB::table('areas')
              ->select('areas.*')
              ->where('area_type', '=', 'country')
              ->orderBy('area_name', 'asc')->get();

    // Show all states for advanced search
  $states = DB::table('areas')
              ->select('areas.*')
              ->where('area_type', '=', 'state')
              ->orderBy('area_name', 'asc')->get();

    return view('index', compact('projects', 'countries', 'states'));
}

public function advanced(Request $request) {
  if($request->ajax())
  {

    // set the variables from the form
    $keyword = $request->input('keyword');
    $city = $request->input('city');
    $country = $request->input('country');
    $state = $request->input('state');
    $timerange1 = $request->input('timerange1');
    $timerange2 = $request->input('timerange2');

    $stateCheck = is_null($state); // check if state is null or not

    $projects = DB::table('projects')
                ->select('projects.*')->distinct()
                ->leftJoin('tags_projects as r', 'r.project_id', '=', 'projects.project_id')
                ->leftjoin('tags as t', 't.id', '=', 'r.tag_id')
                ->leftjoin('areas_projects as e', 'e.project_id', '=', 'projects.project_id')
                ->leftjoin('areas as a', 'a.area_id', '=', 'e.area_id')

                ->when($city, function ($query1, $city) {
                    return $query1->where('description', 'LIKE', '%'.$city.'%');
                })

                ->when($country && $stateCheck, function ($query2) use($country, $stateCheck) {
                    return $query2->where('area_name', '=', $country);
                })

                ->when($state, function($query3, $state) {
                  return $query3->where('area_name', '=', $state);
                })

                ->when($country && $state, function($query5) use ($state) {
                  return $query5->where('area_name', '=', $state);
                })

                ->when($timerange1 && $timerange2, function ($query4) use ($timerange1, $timerange2) {
                    return $query4->whereBetween('year_start', array($timerange1, $timerange2));
                })

                // if keyword is entered
                // run this function
                // where keyword is either like:
                // name OR
                // tagname OR
                // description OR
                // creator

                ->when($keyword, function($query5, $keyword) {
                  return $query5->where(function($query6) use ($keyword) {
                    $query6->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orwhere('tagname', 'LIKE', '%'.$keyword.'%')
                    ->orwhere('description', 'LIKE', '%'.$keyword.'%')
                    ->orwhere('creator', 'LIKE', '%'.$keyword.'%');
                  });
                })

                ->orderBy('name', 'asc'); // Need to include so that pagination will account for distinct.

      $projects = $projects->paginate(8, ['projects.project_id']);
      return view('data', compact('projects'));
    }
  }
}
