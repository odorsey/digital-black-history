<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Upload;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ImportController extends Controller
{
    // Manual data upload
    public function uploadManualData(Request $request) {
      $inputProjectName = $request->inputProjectName;
      $inputProjectLink = $request->inputProjectLink;
      $inputProjectDesc = $request->inputProjectDesc;
      $inputProjectCreator = $request->insertProjectCreator;
      $inputProjectStatus = $request->inputProjectStatus;
      $inputProjectVolunteers = $request->inputProjectVolunteers;
      $inputProjectHelp = $request->inputProjectHelp;
      $inputProjectNeeds = $request->inputProjectNeeds;
      $inputProjectCreationYear = $request->inputProjectCreationYear;
      $inputProjectStartYear = $request->inputProjectStartYear;
      $inputProjectEndYear = $request->inputProjectEndYear;

      $projectValues = array(
        // don't include project_id since it is auto-increment
        "name" => $request->inputProjectName,
        "link" => $request->inputProjectLink,
        "description" => $request->inputProjectDesc,
        "creator" => $request->inputProjectCreator,
        "status" => $request->inputProjectStatus,
        "volunteers" => $request->inputProjectVolunteers,
        "how_help" => $request->inputProjectHelp,
        "needs" => $request->inputProjectNeeds,
        "year_created" => $request->inputProjectCreationYear,
        "year_start" => $request->inputProjectStartYear,
        "year_end" => $request->inputProjectEndYear
        // don't include updated_at since DB will add automatically
      );

      // If there are values in the array, then...
      // if (!empty($projectValues)) {
        // Insert into MySQL database
        Upload::insertDataManual($projectValues);

        // Return to upload screen with success message
        return redirect('/dashboard/add-project-2');
      // } else {
      //   // Return to upload screen with error message
      //   return redirect('/dashboard/add-project');
      //   // display an error
      //   Session::flash('message', 'Nah, that aint it.');
      // }
    }

  // Get information from databases for next set of queries on add-project-2
  public function addProject2(Request $request) {
    // Retrieve information from project you just added
    $newProject = DB::table('projects')
                  ->select('projects.*')
                  ->orderBy('updated_at', 'desc')->limit(1)->get(); // get most recent timestamp

    // Show all countries for advanced search
    $countries = DB::table('areas')
                ->select('areas.*')
                ->where('area_type', '=', 'country')
                ->orderBy('area_name', 'asc')->get();

      // Show all states for advanced search
    $states = DB::table('areas')
                ->select('areas.*')
                ->where('area_type', '=', 'state')
                ->orderBy('area_name', 'asc')->get();

      return view('/dashboard/add-project-2', compact('newProject', 'countries', 'states'));
  }

  public function uploadManualData2(Request $request){
    // getting ID of last project submitted. Probably a better way to handle this...
    $newProjectID = DB::table('projects')
      ->select('projects.project_id')
      ->orderBy('updated_at', 'desc')->limit(1)->get();

    // Isolate the Project ID
    $newProjectID = json_decode(json_encode($newProjectID), true);

    // Convert the array into a string
    $newProjectID = implode(',', $newProjectID[0]);

      // Set a variable for each country or state that is entered into the form
      $project_country = $request->input('inputProjectCountry');
      $project_state = $request->input('inputProjectState');

      // Associate those values from the form with the values in the DB
      $area_state = DB::table('areas')
                  ->select('areas.area_id')
                  ->where('area_name', '=', $project_state)->limit(1)->get();

      // Looping through the array values
      function recursive($newProjectID, $project_country, $project_state){

        foreach($project_country as $value) {
            $area_country = DB::table('areas')
                        ->select('areas.area_id')
                        ->where('area_name', '=', $value)->get();

            // set up an array with the location area_ids
            $areas[] = $area_country;
      }

      // Now get the US state locations
      foreach($project_state as $value) {
          $area_state = DB::table('areas')
                      ->select('areas.area_id')
                      ->where('area_name', '=', $value)->get();

          // Push to $areas array (which already contains country values)
          array_push($areas, $area_state);

          // Isolate the area_id and convert to a string
          $locations_array = json_decode(json_encode($areas), true);

          // Convert the multidimensional array to a single one in two steps
          $final_locations_array = array_column($locations_array, '0');
          $final_locations_array = array_column($final_locations_array, 'area_id');
    }

      // Loop through $final_locations_array values and add to array for uploading
      foreach($final_locations_array as $location_value) {

        $projectLocationValues = array(
          // don't include area_project_id since it is auto-increment

          "area_id" => $location_value, // set to the area_id of the location that is selected.
          "project_id" => $newProjectID

          // don't include created_at or updated_at since DB will add automatically
        );

        // Finally, upload each area_id and project_id pairing to the database!
        Upload::insertProjectLocation($projectLocationValues);
    }
  }

    // If there is a value from the form, run function
    if($project_country || $project_state) {

      // Get location out of the function
      $location_value = recursive($newProjectID, $project_country, $project_state);
    }

    // Starting tag code
      // First, get the tags input and seprate all of the comma separated entries
      $inputTags = explode(',', $request->input('inputTags'));
        // then run a for each statement to check if they already exist in the tags database
        foreach ($inputTags as $tag) {

          // Strip the white space from each term
          $tag = trim($tag);

          // check to see if each term is in the database
          $tagCheck = DB::table('tags')
                      ->select('tags.id')
                      ->where('tagname', '=', $tag)
                      // ->where("tagname","LIKE", $tag)
                      ->get();
          echo $tag . " = " . $tagCheck . "<br>";

          // isolate the tag id
          $tagIDs[] = $tagCheck;
          $tagIDs_array = json_decode(json_encode($tagIDs), true);
          // Convert the multidimensional array to a single one in two steps
          $final_tagIDs_array = array_column($tagIDs_array, '0');
          $final_tagIDs_array = array_column($final_tagIDs_array, 'id');

          // if a term is not in the database,
          if ((count($tagCheck) === 0)) {
            echo $tag . " is NOT in the database <br>";

            // create a new id and insert it into the tags database
              // find the highest tag ID
              $maxTagID = DB::table('tags')
                          ->max('tags.id');

           //  set to the latest tag ID + 1
            $newTagID = $maxTagID + 1;

          // upload new tag to tags database
            $tagIDValues = array (
                "id" => $newTagID,
                "tagname" => $tag,
            );

            Upload::insertTagIDs($tagIDValues);

            // insert the tag/project pair into the database
            $projectTagValues = array (
              "tag_id" => $newTagID,
              "project_id" => $newProjectID,
            );

            echo $tag . " + projectTagValues = ";
            print_r($projectTagValues);
            echo "<br>";

            // Finally, upload each tag_id and project_id pairing to the database!
            Upload::insertProjectTags($projectTagValues);
          }

          // if a term is in the database,
          else {
            // do not create a new id for it.
            // insert the tag/project pair into the tags_projects database
            foreach ($final_tagIDs_array as $tagID_value) {
              $projectTagValues = array (
                "tag_id" => $tagID_value,
                "project_id" => $newProjectID,
              );
            }
            echo $tag . " + projectTagValues = ";
            print_r($projectTagValues);
            echo "<br>";

            // Finally, upload each tag_id and project_id pairing to the database!
            Upload::insertProjectTags($projectTagValues);
          }
        }
        return redirect('/dashboard')->with('success', 'Project Successfully Added!');
    // }
  }

  // CSV File upload
    public function uploadData(Request $request) {

      // Handle File upload
      // If the file is uploaded
     if ($request->hasFile('file-import')) {
        $file = $request->file('file-import');

        // Get File Details
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $tempPath = $file->getRealPath();
        $fileSize = $file->getSize();
        $mimeType = $file->getMimeType();

        // Valid File Extension - CSV
        $valid_extension = array("csv");

        // Setting Max file size - 2 MB
        $maxFileSize = 2097152;

        // Check file extension
        // If it matches the valid extension, then import
        if (in_array(strtolower($extension), $valid_extension)) {
          // File size validation
          if ($fileSize <= $maxFileSize) {

            // File upload location
            $location = 'uploads';

            // Upload file to "uploads" folder for safe keeping
            $file->move($location,$filename);

            // Set the filepath to where the file is
            $filepath = public_path($location."/".$filename);

            // Read the file
            $file = fopen($filepath, "r");

            $importData_arr = array();
            $i = 0;

            while(($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
              $num = count($filedata);

              for($c=0; $c < $num; $c++) {
                $importData_arr[$i][] = $filedata[$c];
              }
              $i++;
            }
            fclose($file); // got what we needed, so close the file
          }

          // Set up an array with file values
          foreach($importData_arr as $importData) {
            $insertData = array(
              // don't include project_id since it is auto-increment
              "name" => $importData[1],
              "link" => $importData[2],
              "description" => $importData[3],
              "creator" => $importData[4],
              "status" => $importData[5],
              "volunteers" => $importData[6],
              "how_help" => $importData[7],
              "needs" => $importData[8]
              // don't include updated_at since DB will add automatically
            );

            // Insert into MySQL database
            Upload::insertData($insertData);
          }
          Session::flash('message','Import Successful!');
        } else {
          // display an error
          Session::flash('message', 'Nah, that aint it.');
        }

      } else {
        // display an error
        Session::flash('message', 'That did not work :(');
      }

      // Return to upload screen with success message
      // TO-DO: Need to route this to a success page with a link to dashboard
      return redirect('/dashboard')->with('success', 'File Uploaded');
  }

  // Autocomplete functionality for adding tags
  // public function autocomplete(Request $request) {
  //   // Check term against the tags in the database
  //   $suggestion = DB::table('tags')
  //                 ->select('tags.tagname')
  //                 ->where("tagname","LIKE","%{$request->term}%")
  //                 ->get();
  //
  //   return response()->json($suggestion);
  // }
}
