<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index() {
      return view('index');
    }

    public function about() {
      $data = array(
        'title' => 'About'
      );
      return view('about')->with($data);
    }

    public function faqs() {
      $data = array(
        'title' => 'Frequently Asked Questions'
      );
      return view('faqs')->with($data);
    }

    public function support() {
      $data = array(
        'title' => 'Support the Project'
      );
      return view('support')->with($data);
    }

    public function contact() {
      return view('contact');
    }

    public function admin() {
      return view('dashboard');
    }

    public function register() {
      return view('auth/register');
    }

    public function login() {
      return view('auth/login');
    }

    public function addProject() {
      return view('dashboard/add-project');
    }

    public function addProject2() {
      return view('dashboard/add-project-2');
    }

    public function editProject() {
      return view('dashboard/edit-project');
    }
}
