<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Project;
use Illuminate\Support\Facades\Input;

Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/faqs', 'PagesController@faqs');
Route::get('/support', 'PagesController@support');
Route::resource('projects', 'ProjectsController');
Route::get('/', 'SearchController@index');
Route::get('/search/view', 'PagesController@search');
Route::get('/search', 'SearchController@advanced');
Route::post('/search', 'SearchController@advanced');
// Route::get('/contact', 'PagesController@contact');
Route::get('/contact', 'ContactFormController@create');
// Route::post('/advanced', 'SearchController@advanced');
// Route::get('/advanced', 'SearchController@advanced');
// Route::get('/advanced/view', 'SearchController@advanced');
// Route::get('/help', 'ProjectsController@index');
Route::post('/contact', function(Request $request){
  Mail::send(new ContactMail($request));
  return redirect('/');
});

Route::get('/dashboard', 'PagesController@admin');
Route::get('/dashboard/login', 'PagesController@login');
Route::get('/dashboard/add-project', 'PagesController@addProject');
Route::get('/dashboard/add-project-2', 'PagesController@addProject2');
Route::get('/dashboard/add-project-2', 'ImportController@addProject2');
// Route::get('/dashboard/register', 'PagesController@register');
// Route::get('/admin/add-project-2', 'ImportController@autocomplete')->name('autocomplete');
Route::post('/uploadManualData', 'ImportController@uploadManualData'); // manual upload - step 1
Route::post('/uploadManualData2', 'ImportController@uploadManualData2'); // manual upload - step 2
Route::post('/uploadData', 'ImportController@uploadData'); // CSV upload

Route::get('/dashboard/edit-project', 'PagesController@editProject');


// // Clearing Laravel Configuration Cache: if you get the file_put_content error upon page load
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
